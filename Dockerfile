# Usando a imagem oficial do Nginx
FROM nginx:alpine

# Copiando os arquivos de configuração do Nginx para o contêiner
COPY nginx.conf /etc/nginx/nginx.conf

# Copiando os arquivos do site para o diretório padrão do Nginx
COPY html /usr/share/nginx/html

# Expondo a porta 80 para acesso HTTP
EXPOSE 80

# Iniciando o Nginx quando o contêiner for iniciado
CMD ["nginx", "-g", "daemon off;"]
